# NodeJs Authentication Boilerplate

This repo contains code for Local email-passport authentication and Google Authentication.
This Website Authentication template can be used for the authentication and session creation for any website. 
This project is built using Html(EJS Template), Javascript, Node JS, Express JS, Passport JS, Express JS layouts, MongoDb, passport-local, Cookie parser, Express Session, Bcrypt and mongoose while keeping the integrity of MVC scalable file structure.

## USAGE

### Install All Packages

```bash
npm install express ejs mongoose bcryptjs connect-flash cookie-parser express-session csurf memorystore passport passport-local passport-google-oauth20 nodemailer
```

### Install Nodemon For Development

```bash
npm install -D nodemon
```

### Add mongoURI ,Google client ID and Secret, smtp config for sending emails 

## To get clientID and clientSecret refer link:

https://developers.google.com/adwords/api/docs/guides/authentication

### The Server should run at: 

http://localhost:8000/

